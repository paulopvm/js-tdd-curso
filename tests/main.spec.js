import {expect} from 'chai';
import {sum, sub, mult, div} from '../src/main.js';
describe('Calc', () => {
    describe( 'Smoke tests', () => {
        it('shoud exist the method sun', () => {
            expect(sum).to.exist;
            expect(sum).to.be.a.functtion;
        });
        it('shoud exist the method sub', () => {
            expect(sub).to.exist;
            expect(sub).to.be.a.functtion;
        });
        it('shoud exist the method mult', () => {
            expect(mult).to.exist;
            expect(mult).to.be.a.functtion;
        });
        it('shoud exist the method div', () => {
            expect(div).to.exist;
            expect(div).to.be.a.functtion;
        });
    });

    describe('Sum', () => {
        it('shoud return 4 when sum(2,2)', () => {
            expect(sum(2,2)).to.be.equal(4);
        });
    });

    describe('Sub', () => {
        it('shoud return 4 when sub(6,2)', () => {
            expect(sub(6,2)).to.be.equal(4);
        });
        it('shoud return -4 when sub(6,10)', () => {
            expect(sub(6,10)).to.be.equal(-4);
        });
    });

    describe('Mult', () => {
        it('shoud return 4 when mult(2,2)', () => {
            expect(mult(2,2)).to.be.equal(4);
        });
    });

    describe('Div', () => {
        it('shoud return 1 when div(2,2)', () => {
            expect(div(2,2)).to.be.equal(1);
        });

        it('should return `não é possível divisão por zero` when divide by zero', () => {
            expect(div(2,0)).to.be.equal('não é possível divisão por zero');
        });
    });
});